//
//  TodayViewController.swift
//  YawzaWidget
//
//  Created by Maxim Shmotin on 24/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import NotificationCenter
import BtceApiKit

class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet var LastBuy: UILabel!
    @IBOutlet var LastSell: UILabel!
    private var tracker: NSTimer!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        NSLog("Widget start")
        tracker = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        self.preferredContentSize = CGSizeMake(0, 90)
        completionHandler(NCUpdateResult.NewData)
    }
    
    func update(){
        NSLog("Update start")
        ApiHandler.getBuySellRate(){ (buy,sell) in
            if let buyRate = buy{
                if let sellRate = sell{
                    self.LastSell.text = "$\(buyRate)"
                    self.LastBuy.text = "$\(sellRate)"
                }
            }
        }
        
    }
    
    @IBAction func openYawzaBot (sender: UIButton){
        let url: NSURL = NSURL(string: "com.YawzaBot.app://")!
        self.extensionContext?.openURL(url, completionHandler: nil)
    }
}

