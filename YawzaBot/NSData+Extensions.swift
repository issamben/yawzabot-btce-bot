//
//  NSData+Extensions.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 04/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

extension NSData{
    func getJsonFromNSData() -> NSDictionary?{
        if let json = try! NSJSONSerialization.JSONObjectWithData(self, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary{
            return json
        }else{
            return nil
        }
    }
}