//
//  FundsViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 20/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class FundsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    private var fundsDictinary = Dictionary<String, Double>()
    private var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tableViewController = UITableViewController()
        tableViewController.tableView = self.tableView
        
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: "updateRefreshControll", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl?.backgroundColor = UIColor.blackColor()
        refreshControl?.tintColor = UIColor.whiteColor()
        
        tableViewController.refreshControl = self.refreshControl
        
        tableView.registerNib(UINib(nibName: "FundsCell", bundle: nil), forCellReuseIdentifier: "fundsCellView")
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        update()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        SVProgressHUD.dismiss()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //tableView
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("fundsCellView", forIndexPath: indexPath) as! FundsCustomCell
        let key = Array(self.fundsDictinary.keys)[indexPath.row].uppercaseString
        cell.currencyTitleLabel.text = key
        cell.amountLabel.text = "Balance: \(Array(self.fundsDictinary.values)[indexPath.row]) \(key)"
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fundsDictinary.count
    }
    
    private let HEADER_ROW_HEIGHT: CGFloat = 50
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return HEADER_ROW_HEIGHT
    }
    
    //Logic
    
    func update(){
        SVProgressHUD.showWithStatus("Updating Funds...")
        fundsDictinary.removeAll(keepCapacity: false)
        self.tableView.reloadData()

        if (Reachability.isConnectedToNetwork()){
            self.updateBalance { success in
                if (success){
                    dispatch_async(dispatch_get_main_queue()) {
                        SVProgressHUD.dismiss()
                        self.tableView.reloadData()
                    }
                }
            }
        }else{
            SVProgressHUD.showErrorWithStatus("No Internet Connection!")
        }
    }
    
    func updateRefreshControll(){
        let dataFormat = NSDateFormatter()
        dataFormat.dateFormat = "MMM-d HH:mm:ss"
        let title = "Last update: \(dataFormat.stringFromDate(NSDate()))"
        let arrtibutedTitle = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        refreshControl.attributedTitle = arrtibutedTitle
        
        update()
        
        refreshControl?.endRefreshing()
    }
    
    func updateBalance(completionHandler: Bool -> Void){
        print("run update balance")
        dispatch_async(BtceApiHandler.sharedInstanse.dispatch_serial_queue){
            if let json = BtceApiHandler.sharedInstanse.updateAccountInfo(){
                if let info = json["return"] as? NSDictionary, funds = info["funds"] as? NSDictionary{
                    self.fundsDictinary = funds as! Dictionary
                    completionHandler(true)
                }else if let _ = json["error"] as? String{
                    dispatch_async(dispatch_get_main_queue()) {
                        Dialog.show(Dialog.Title.ERROR, message: (json["error"] as! String).capitalizeFirst)
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
}
