//
//  OrdersViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 05/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    private var refreshControl: UIRefreshControl!
    private var pickerVC = RMPickerViewController.init(style: RMActionControllerStyle.White, selectAction: nil, andCancelAction: nil)

    private var tracker: NSTimer!
    private var openOrders: [Order] = []
    private var currentTradePair = Currency.getCode()

    override func viewDidLoad() {
        super.viewDidLoad()

        let tableViewController = UITableViewController()
        tableViewController.tableView = self.tableView
        
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: "updateRefreshControll", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl?.backgroundColor = UIColor.blackColor()
        refreshControl?.tintColor = UIColor.whiteColor()
        tableViewController.refreshControl = self.refreshControl
        
        pickerVC.title = "Currency Code"
        pickerVC.picker.delegate = self
        pickerVC.picker.dataSource = self
        pickerVC.picker.resignFirstResponder()
        
        let cancelAction = RMAction(title: "Cancel", style: RMActionStyle.Cancel, andHandler: nil)
        
        let selectAction = RMAction(title: "Done", style: RMActionStyle.Done) { controller in
            let pickerView = (controller as! RMPickerViewController).picker
            self.currentTradePair = Currency.getCode(pickerView.selectedRowInComponent(0))
            self.update()
        }
        pickerVC.addAction(cancelAction)
        pickerVC.addAction(selectAction)

        tableView.registerNib(UINib(nibName: "OrdersHeader", bundle: nil), forCellReuseIdentifier: "ordersHeaderView")
        tableView.registerNib(UINib(nibName: "OrdersCell", bundle: nil), forCellReuseIdentifier: "ordersCellView")
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        update()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        SVProgressHUD.dismiss()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // Table view methods

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return openOrders.count
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier("ordersHeaderView") as! UIView
        let uiview = UIView()
        uiview.addSubview(cell)
        tableView.tableHeaderView = uiview
        return uiview
    }
    
    private let HEADER_ROW_HEIGHT: CGFloat = 40
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HEADER_ROW_HEIGHT
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return HEADER_ROW_HEIGHT
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ordersCellView", forIndexPath: indexPath) as! OrdersCustomCell
        cell.type.text = openOrders[indexPath.row].type?.rawValue
        cell.type.textColor = Order.getColorForType(openOrders[indexPath.row].type!)
        cell.price.text = "\(openOrders[indexPath.row].rate)"
        cell.amount.text = "\((openOrders[indexPath.row].amount).format())"
        cell.total.text = "\((openOrders[indexPath.row].total).format())"
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete && segmentedControl.selectedSegmentIndex == 0 {
            // Delete the row from the data source
            BtceApiHandler.sharedInstanse.cancelActiveOrder(openOrders[indexPath.row].id)
            self.statusLabel.text = "\(self.openOrders.count) orders"
            self.openOrders.removeAtIndex(indexPath.row)
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            self.tableView.reloadData()
        }
    }
    
    // Picker View
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Currency.getCodesCount()
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(Currency.toCodeTitle(row))"
    }
    
    @IBAction func changeTradePair(sender: UIButton){
        self.presentViewController(pickerVC, animated: true, completion: nil)
    }
    
    /// Logic
    
    func updateRefreshControll(){
        let dataFormat = NSDateFormatter()
        dataFormat.dateFormat = "MMM-d HH:mm:ss"
        let title = "Last update: \(dataFormat.stringFromDate(NSDate()))"
        let arrtibutedTitle = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        self.refreshControl?.attributedTitle = arrtibutedTitle
        
        update()
        
        self.refreshControl?.endRefreshing()
    }
    
    func update(){
        self.statusLabel.text = "\(self.currentTradePair.uppercaseString)"
        openOrders.removeAll(keepCapacity: false)
        self.tableView.reloadData()
        
        if (Reachability.isConnectedToNetwork()){
            
            if (segmentedControl.selectedSegmentIndex == 0){
                updateActiveOrdersInfo(currentTradePair)
            }else{
                updateOrdersHistoryInfo(currentTradePair)
            }
        }else{
            SVProgressHUD.showErrorWithStatus("No Internet Connection!")
        }
    }
    
    func updateActiveOrdersInfo(code: String){
        SVProgressHUD.showWithStatus("Updating Active Orders...")
        dispatch_async(BtceApiHandler.sharedInstanse.dispatch_serial_queue){
            if let json = BtceApiHandler.sharedInstanse.getActiveOrders(code){
                if ((json["success"] as? Int) == 0){
                    dispatch_async(dispatch_get_main_queue()) {
                        Dialog.show(Dialog.Title.ERROR, message: (json["error"] as! String).capitalizeFirst)
                        SVProgressHUD.dismiss()
                    }
                }else{
                    self.addOrder(json, withTimestamp: "timestamp_created")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    
    /// Sort by timestamp
    func updateOrdersHistoryInfo(code: String){
        SVProgressHUD.showWithStatus("Updating Orders History...")        
        dispatch_async(BtceApiHandler.sharedInstanse.dispatch_serial_queue){
            if let json = BtceApiHandler.sharedInstanse.getTradeHistory(code){
                if ((json["success"] as? Int) == 0){
                    dispatch_async(dispatch_get_main_queue()) {
                        Dialog.show(Dialog.Title.ERROR, message: (json["error"] as! String).capitalizeFirst)
                        SVProgressHUD.dismiss()
                    }
                }else{
                    self.addOrder(json, withTimestamp: "timestamp")
                    dispatch_async(dispatch_get_main_queue()) {
                        self.tableView.reloadData()
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    
    func addOrder(json: NSDictionary, withTimestamp: String){
        if let orders = json["return"] as? NSDictionary {
            for id in orders {
                if (!isContansOrderID(Int(id.key as! String)!)){
                    self.openOrders.append(Order(id: Int(id.key as! String)!,
                        rate: id.value["rate"] as! Double!,
                        amount: id.value["amount"]! as! Double!,
                        timestamp: id.value[withTimestamp]! as! Int!,
                        type: Order.checkType(id.value["type"]! as! String!)))
                    self.openOrders.sortInPlace({ $0.timestamp > $1.timestamp })
                }
            }
        }
    }
    
    /// Check if order is already added
    func isContansOrderID(id: Int) -> Bool{
        for order in openOrders{
            if (order.id == id){
                return true
            }
        }
        return false
    }
    
    /// Check if order is executed
    /// TODO: save the last added orders and check them at the app start
    func isOrderExecuted(json: NSDictionary){
        if let orders = json["return"] as? NSDictionary {
            for id in orders{
                if let type = id.value["type"]! as? String, pair = id.value["pair"]! as? String,
                    amount = id.value["amount"]! as? Double, rate = id.value["rate"]! as? Double where id.value["status"]! as! Int == 1 {
                        Dialog.show(Dialog.Title.SUCCESS, message: "Order is executed. Type: \(type.uppercaseString) \n  Pair: \(pair) \n Amount: \(amount) \n Rate: \(rate)")
                }
            }
        }
    }
    
    @IBAction func switchOrderState(sender: UISegmentedControl){
        SVProgressHUD.show()
        update()
    }
}
