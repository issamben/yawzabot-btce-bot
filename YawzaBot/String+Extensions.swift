//
//  String+Extensions.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 04/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

extension String{
    
    var capitalizeFirst: String {
        var result = self
        result.replaceRange(startIndex...startIndex, with: String(self[startIndex]).capitalizedString)
        return result
    }
    
    var length : Int {
        return self.characters.count
    }
    
    subscript (r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = startIndex.advancedBy(r.endIndex)
        return substringWithRange(Range(start: start, end: end))
    }
    
    func containSpecialCharacters() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: [])
        return regex.firstMatchInString(self, options:[],
            range: NSMakeRange(0, utf16.count)) != nil
    }
    
    func extractBitcoinAddress() -> String{
        var resultString = ""
        
        resultString = self.stringByReplacingOccurrencesOfString("/", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        if (resultString.containsString(":")){
            let rangeToSymbol = resultString.rangeOfString(":")
            let index: Int = resultString.startIndex.distanceTo(rangeToSymbol!.startIndex)
            resultString = resultString.substringFromIndex(resultString.startIndex.advancedBy(index+1))
        }
        
        if resultString.containsString("?"){
            let rangeToSymbol = resultString.rangeOfString("?")
            let index: Int = resultString.startIndex.distanceTo(rangeToSymbol!.startIndex)
            resultString = resultString.substringToIndex(resultString.startIndex.advancedBy(index))
        }
        print("Result string: \(resultString)")
        return resultString
    }

}
