//
//  QuotesViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 17/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class QuotesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    private let OPERATION_VIEW_ID: NSString = "OperationView"
    private var selectedTradePair: String?
    private var tracker: NSTimer!
    private var refreshControl: UIRefreshControl!

    var myString = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*if (Authentication.userPasswordSet()){
            Authentication.authenticateUser(self)
        }*/

        let tableViewController = UITableViewController()
        tableViewController.tableView = self.tableView
        
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: "update", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl?.backgroundColor = UIColor.blackColor()
        refreshControl?.tintColor = UIColor.whiteColor()
        
        tableViewController.refreshControl = self.refreshControl
        
        tableView.registerNib(UINib(nibName: "QuotesCell", bundle: nil), forCellReuseIdentifier: "quotesCellView")
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        checkInternetConnection()
        if (Reachability.isConnectedToNetwork()){
            updateAllTradePairs()
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkInternetConnection(){
        if (!Reachability.isConnectedToNetwork()){
            let alert = UIAlertController(title: "Warning", message: "No Internet Connection!", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //tableView
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("quotesCellView", forIndexPath: indexPath) as! QuotesCustomCell
        cell.currencyCode.text = Currency.toMyCodeTitle(indexPath.row)
        
        if let data = quotesDictionary[Currency.getMyCode(indexPath.row)] as TradePairData? {
            cell.lastAsk.text = data.lastAsk
            cell.lastBid.text = data.lastBid
            cell.high.text = data.high
            cell.low.text = data.low
            cell.spread.text = data.spread
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Currency.getMyCodesCount()
    }
    
    private let HEADER_ROW_HEIGHT: CGFloat = 50
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return HEADER_ROW_HEIGHT
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedTradePair = Currency.getMyCode(indexPath.row)
        performSegueWithIdentifier(OPERATION_VIEW_ID as String, sender: nil)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            Currency.removeAtIndex(indexPath.row)
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            self.tableView.reloadData()
        }
    }
    
    // Load data
    internal class TradePairData{
        internal var lastAsk, lastBid, high, low, spread: String?
        init(lastAsk: String, lastBid: String, high: String, low: String, spread: String){
            self.lastAsk = lastAsk
            self.lastBid = lastBid
            self.high = high
            self.low = low
            self.spread = spread
        }
    }
    
    private var quotesDictionary = Dictionary<String, TradePairData>()
    
    func updateAllTradePairs(){
        let queue: dispatch_queue_t  = dispatch_queue_create("updateQueue", DISPATCH_QUEUE_CONCURRENT)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            dispatch_apply(Int(Currency.getMyCodesCount()), queue) { number -> Void in
            
                let currency = Currency.getMyCode(Int(number))
                self.updateTradePairRate (currency) { (ask, bid, high, low) -> Void in
                    let tradePairdata = TradePairData(lastAsk: "\(ask.format())", lastBid: "\(bid.format())",
                        high: "High: \(high.format())", low: "Low: \(low.format())", spread: "Spread: \((ask-bid).format())")
                    self.quotesDictionary[currency] = tradePairdata
                }
                return
            }
            
            dispatch_async(dispatch_get_main_queue()){
                NSLog("Reload_Data")
                self.tableView.reloadData()
            }
        }
    }
    
    //update
    
    func update(){
        
        let dataFormat = NSDateFormatter()
        dataFormat.dateFormat = "MMM-d HH:mm:ss"
        let title = "Last update: \(dataFormat.stringFromDate(NSDate()))"
        let arrtibutedTitle = NSAttributedString(string: title, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        self.refreshControl?.attributedTitle = arrtibutedTitle
        
        updateAllTradePairs()
        
        self.refreshControl?.endRefreshing()
        
    }
    
    func updateTradePairRate(tradePair: String, completionHandler: (Double, Double, Double, Double) -> Void ){
            if let json = BtceApiHandler.sharedInstanse.getTradePairRate(tradePair){
                let ticker: AnyObject! = json[tradePair]
                let bid = ticker["sell"] as! Double
                let ask = ticker["buy"] as! Double
                
                if (tradePair == "btc_usd"){
                    self.saveRateValuesForWidget(bid, sell: ask)
                }

                completionHandler(ask, bid , ticker["high"] as! Double, ticker["low"] as! Double)
        }
    }
    
    /// Buttons
    @IBAction func setupTouchIDAuth(){
        performSegueWithIdentifier("passwordSegue", sender: nil)
    }
    
    @IBAction func addTradePairCode(){
        performSegueWithIdentifier("chooseTradePairs", sender: nil)
    }

    // Widget
    private func saveRateValuesForWidget(buy: Double, sell: Double){
        let defaults = NSUserDefaults(suiteName: "group.YawzaBot.Widget")
        defaults?.setObject("\(buy)", forKey: "buyRate")
        defaults?.setObject("\(sell)", forKey: "sellRate")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == OPERATION_VIEW_ID){
            let vc = segue.destinationViewController as! TradeViewController
            vc.tradePair = TradePair(code: selectedTradePair!)
        }
    }
    }
